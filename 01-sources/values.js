const pull = require('pull-stream')

// This function takes an Array of items and feeds them one at a time
// through the pipe.
//
// I use this in tests mainly, for queuing up the publishing of several
// messages in a row (with pull.asyncMap for example)

pull(
  pull.values([1, 2, 3, 4, 5]), // source
  pull.drain(value => { //      // sink
    console.log(value)
  })
)
