

  db.getUserStream(),                // source
  pull.filter(user => user.isAdmin), // through
  pull.map(user => user.name),       // through
  pull.collect((err, names) => {     // sink
    if (err) return console.error('Oh no!', err)

    console.log('Admin users:', names)
  })
)
