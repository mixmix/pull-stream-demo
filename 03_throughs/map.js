const pull = require('pull-stream')

// Behaves like Array.prototype.map

pull(
  pull.values([1, 2, 3, 4, 5]),
  pull.filter(value => value % 2 === 0), // through
  pull.drain(value => console.log(value))
)
// =>
//   2
//   4
//   6
//   8
//   10
