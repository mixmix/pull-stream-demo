const pull = require('pull-stream')

// Only want a couple results?
// This is great if you have:
//   - a live stream which you want to close once you get to n values
//   - getting just the latest values from a stream

pull(
  pull.values([1, 2, 3, 4, 5]),
  pull.take(2),
  pull.drain(value => console.log(value))
)
// =>
//   1
//   2
