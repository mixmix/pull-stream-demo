const pull = require('pull-stream')

// A through which is like pull.map, but allows for async calls (e.g. database
// or network calls)
// This function is fine, but you should probably consider pull-paramap if you
// are processing many recordsm - it's often much faster.

pull(
  pull.values([1, 2, 3, 4, 5]),
  pull.asyncMap((value, cb) => { // through
    // do somethng with the value, then:
    // - call cb(err) if there was an error
    // - call cb(null, result) with result
    doublingApi(value, cb)
  }),
  pull.drain(value => console.log(value))
)
// =>
//   2
//   4
//   6
//   8
//   10
//   done null

function doublingApi (value, cb) {
  console.log(`api call (${value})`)
  setTimeout(
    () => cb(null, 2 * value),
    1000 * Math.random()
  )
}
