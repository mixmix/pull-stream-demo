const pull = require('pull-stream')

// Behaves like Array.prototype.filter - only things which return true are
// allowed to proceed through the stream

pull(
  pull.values([1, 2, 3, 4, 5]),
  pull.filter(value => value % 2 === 0), // through
  pull.drain(value => console.log(value))
)
// =>
//   2
//   4
