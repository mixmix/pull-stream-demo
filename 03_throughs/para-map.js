const pull = require('pull-stream')
const pullParaMap = require('pull-paramap')

// This is like pull.asyncMap EXCEPT it runs async calls in parallel!
// NOTE you have to supply a second argument "width" which defines the maximum
// number of parallel async actions that can be running at any time.
//   - width = 1 makes this same as pull.asyncMap
//   - width = 5 is good
//   - width = 100 is probably too much going on, but you should experiment
//     with your context

pull(
  pull.values([1, 2, 3, 4, 5]),
  pullParaMap(
    (value, cb) => doublingApi(value, cb),
    5 // width
  ),
  pull.drain(value => console.log(value))
)
// =>
//   2
//   4
//   6
//   8
//   10
//   done null

function doublingApi (value, cb) {
  console.log(`api call (${value})`)
  setTimeout(
    () => cb(null, 2 * value),
    1000 * Math.random()
  )
}
