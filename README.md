# pull-stream demo

pull-stream let you work with streams of data chunks. It excells for composing
simple asynchronous pipelines.

Each pull stream pipeline is made up of:
  - a source : where the data is coming from
  - throughs (optional) : intermediate bits of pipe which transform data
  - a sink : where the data ends up

```js
pull(
  db.getUserStream(),                // source
  pull.filter(user => user.isAdmin), // through
  pull.map(user => user.name),       // through
  pull.collect((err, names) => {     // sink
    if (err) return console.error('Oh no!', err)

    console.log('Admin users:', names)
  })
)
```

Here `db.getUserStream` is some function which creates a source which produces
chunks of data (a user record per chunk). Each chunk is passed to the next thing
in the pipeline, until it reaches the sink.

## Learning

1. Read through the code examples of the most common sources, sinks, and
   throughs in this repo.

2. check out more functions here: https://pull-stream.github.io/


