const pull = require('pull-stream')

// Drain is sink that is great if you have a long-running, or live
// source and you expect to handle results as you go.

pull(
  pull.values([1, 2, 3, 4, 5]), // source
  pull.map(value => value * 2), // through
  pull.drain( //                // sink
    value => {
      console.log(value)
    },
    // optional second arg that is run on error, or completion of stream
    err => {
      console.log('done', err)
    }
  )
)
// =>
//   2
//   4
//   6
//   8
//   10
//   done null
