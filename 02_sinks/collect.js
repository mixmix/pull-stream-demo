const pull = require('pull-stream')

// This is used if you expect a finite time for result to arrive,
// and need all results before proceeding

pull(
  pull.values([1, 2, 3, 4, 5]), // source
  pull.map(value => value * 2), // through
  pull.collect((err, values) => {
    console.log(err, values)
  })
)
// =>
//   null [2, 4, 6, 8, 10]
