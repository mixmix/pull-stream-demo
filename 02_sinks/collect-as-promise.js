const pull = require('pull-stream')

// Like collect, but great for async-await contexts

async function run () {
  const res = await pull(
    pull.values([1, 2, 3, 4, 5]), // source
    pull.map(value => value * 2), // through
    pull.collectAsPromise()
  )

  console.log(res)
  // =>
  //   [2, 4, 6, 8, 10]
}

run()

// you want to make sure you .catch(err => {})
// OR wrap in a try/catch
